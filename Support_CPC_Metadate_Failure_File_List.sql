Support_CPC_Metadate_Failure_File_List

-- CPC and Metadata failures, query for failed file list, can be used in AWS download script to pull files.

SELECT * FROM imports i
JOIN messages m on i.imports_id = m.imports_id
WHERE i.status_id = 3
AND i.import_date_time > CURDATE() - INTERVAL 12 HOUR
AND m.level = 'ERROR'
AND (m.message LIKE '%CPC%' OR m.message LIKE '%metadata%')
AND NOT EXISTS (SELECT 1 FROM imports i2 WHERE i2.status_id = 1 -- Only look for failed files that have not been reprocessed
                       AND i2.refresh_date_time = i.refresh_date_time
                       AND i2.online_store = i.online_store
                       AND i2.file_type = i.file_type
                       AND i2.vendor = i.vendor
                       AND i2.country = i.country
                       AND i2.imports_id > i.imports_id
                       AND i2.import_file_name REGEXP '.*[[:digit:]]{2}(\.csv)+$' = 1)  -- and aren't targeted loads